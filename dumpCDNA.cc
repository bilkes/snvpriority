/*
 * dumpCDNA.cc
 *
 *  Created on: Nov 21, 2013
 *      Author: sven
 */
#include <sys/stat.h>
#include <fstream>

#include <guli/uiOperationMode.h>
#include <guli/uiParms.h>
#include <guli/bedTiling.h>
#include <guli/gffDag.h>
#include <guli/gffParser.h>
#include "Genome2Feature.h"

class dumpCDNA : public GULI::uiOperationMode {
	public:
		dumpCDNA();
		virtual ~dumpCDNA(){};
		virtual int main(int argc, char **argv);

	protected:
		GULI::uiParms      _parms;
		touchVarT          _overwrite;
		GULI::gffDag       _geneModel;
		GULI::gffNodeListT _cdna;

	private:
		int _getGeneModel(const char *gffFileName, const GULI::gffParser::parserMode &semantic);
};


using namespace GULI;
class DatasetDescriptor {};



dumpCDNA::dumpCDNA() :
	uiOperationMode("dumpCDNA"),
 	_overwrite("overwrite", "Overwrite output file if it exists",  _parms)
{}

int dumpCDNA::main(int argc, char **argv) {
	Genome2Feature g2f("exon");
	g2f.readGff(argv[1]);
	const GULI::constGffNodeListTiling::tileListT &tiles = g2f.cDNA()->getTiling();
	for(size_t i=0; i < tiles.size(); ++i) {
		std::cout << tiles[i].chromosome() << "\t" << tiles[i].start() << "\t" << tiles[i].end() << "\n";
	}

	return 0;
}

static dumpCDNA _myself;
