/*
 * keggDag.cpp
 *
 *  Created on: Oct 18, 2013
 *      Author: sven
 */

#include "keggDag.h"
namespace GULI {
//	keggDag::keggDag() {}

	keggDag::Node::Node(const textual &T) :  utilAttributeFunctionality(T), _id(T.id), _type(T.type) {
		removeAttribute(keggDag::Node::childAttribute);
		removeAttribute("interaction_idref");
	}


	keggDag::~keggDag() {}

	// Initialize an empty gffDag to contain a root element
	void keggDag::clear() {
		Dag::clear();
		_addNode(new Node(rootId,   Node::TECHNICAL));
		_addNode(new Node(orphanId, Node::TECHNICAL));
		return;
	}

	keggDag::Node *keggDag::operator[]   (const string &id) const {
		return dynamic_cast<Node *> (Dag::operator[](id));
	}

	void keggDag::addNode(Node *N)	{
		_addNode(static_cast<Dag::Node *>(N), N->id());
	}

	ostream &keggDag::Node::operator<<(ostream &O) const {
		O << id() << "\t";
		switch(type()) {
			case TECHNICAL:   		O << "TECHNICAL";    break;
			case MOLECULE:    		O << "MOLECULE";     break;
			case PATHWAY:	  		O << "PATHWAY";      break;
			case INTERACTION: 		O << "INTERACTION";  break;
			case MOLECULE_LIST: 	O << "MOLECULE_LIST"; break;
		}
		O << "\t";

		for(size_t i=0; i < knownAttributes().size(); ++i) {
			if(i) O << ",";
			O << knownAttributes()[i] << ":" << attribute(knownAttributes()[i]);
		}
		O << "\t";
		for(size_t i=0; i < nOutgoing(); ++i) {
			if(i) O << "\t";
			O << outgoing(i)->id() << "(";
			bool first = true;
			linkTypeT type;
			type.voidRepresentation = outgoing(i).userdata();
			if(type.bitfield.PART_OF) 		{ O << "PART_OF"; first=false; }
			if(type.bitfield.IS_A)    		{ if(!first) O << ","; O << "IS_A";   first=false; }
			if(type.bitfield.INPUT)   		{ if(!first) O << ","; O << "INPUT";  first=false; }
			if(type.bitfield.OUTPUT)  		{ if(!first) O << ","; O << "OUTPUT"; first=false; }
			if(type.bitfield.AGENT) 		{ if(!first) O << ","; O << "AGENT";  first=false; }
			if(type.bitfield.INHIBITOR)		{ if(!first) O << ","; O << "INHIBITOR";  first=false; }
			O  << ")";
		}
		return O;
	}

	const string keggDag::rootId = "root";
	const string keggDag::orphanId = "orphans";
	const string keggDag::Node::childAttribute 		= "Child";
	const string keggDag::Node::listType		 	= "ListType";
	const string keggDag::Node::listTypeFamily	 	= "GeneFamily";
	const string keggDag::Node::listTypeComplex	 	= "Complex";


}
