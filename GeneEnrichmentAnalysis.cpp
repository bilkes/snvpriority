/*
 * GeneEnrichmentAnalysis.cpp
 *
 *  Created on: Oct 14, 2013
 *      Author: sven
 */

#include "GeneEnrichmentAnalysis.h"

GeneEnrichmentAnalysis::GeneEnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A) : EnrichmentAnalysis(F,A) {}

GeneEnrichmentAnalysis::~GeneEnrichmentAnalysis() {}

int GeneEnrichmentAnalysis::_init() {
	for(unsigned int i=0; i < _knownFeatures().size(); ++i) {
		FeaturePayload *P = static_cast<FeaturePayload *>(_knownFeatures()[i]->getUserVariable());
		assert(P);
		P->idx(i);
	}
	return 0;
}

void  GeneEnrichmentAnalysis::categoryFeatures(size_t category, Genome2Feature::featureListT &result) {
	result.resize(1);
	result[0] = _knownFeatures()[category];
	return;
}

void   GeneEnrichmentAnalysis::featureCategories(Genome2Feature::featureT &F,  categoryListT &result) {
	FeaturePayload *P = static_cast<FeaturePayload *>(F->getUserVariable());
	assert(P);
	result.resize(1);
	result[0] = P->idx();
}


string  GeneEnrichmentAnalysis::categoryDescription(size_t category) {
	string r;
	const Genome2Feature::featureT &F = _knownFeatures()[category];
	r += F->attribute("Name") + "\t";
	r += F->attribute("Type") + "\t";
	r += F->attribute("Status") + "\t";
	r += F->chromosome() + "\t" +  std::to_string(F->start()) + "\t" + std::to_string(F->end());
	return r;
}

