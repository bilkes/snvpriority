/*
 * Genome2Gene.h
 *
 *  Created on: Oct 8, 2013
 *      Author: sven
 */

#ifndef GENOME2GENE_H_
#define GENOME2GENE_H_
#include <guli/gffDag.h>
#include <guli/gffAlgo.h>
#include <guli/gffParser.h>
#include <guli/bedRegion.h>
#include <guli/bedTiling.h>

class Genome2Feature {
public:
	typedef GULI::gffDag::Node *featureT;
	typedef GULI::gffNodeListT featureListT;
	Genome2Feature(const char *regionRegex, const GULI::gffParser::parserMode *M = &GULI::gffParser::Native);
	virtual ~Genome2Feature();
	int readGff(const char *fn);
	void clear();

	void findProbes(const GULI::bedRegion &, featureListT &) const;
	void findFeatures(const GULI::bedRegion &hit, featureListT &T, const char *regionRegex) const;
	void allFeatures(featureListT &T, const char *regionRegex);
	void cDNA(const featureListT &feature, GULI::constGffNodeListTiling::targetListT &r) const;
	void cDNA(const featureListT &feature, GULI::bedRegion::listT &r) const;
	const GULI::constGffNodeListTiling 	*cDNA() const { return _cDNA; }

	GULI::gffDag &genomeAnnotation() { return _annot; }

private:
	size_t                          _newMagicNumber() const;
	GULI::gffDag           			_annot;
	GULI::gffNodeListT	   			_probeList;
	GULI::constGffNodeListTiling 	*_cDNA;
	const char             			*_probeFeatureRegexp;
	const GULI::gffParser::parserMode     *_parserMode;
	mutable vector<size_t> 		   _cDNAseen;
	mutable size_t                 _cDNAmagic;
};

#endif /* GENOME2GENE_H_ */
