/*
  * reduceGffMode.cpp
 *
 *  Created on: Oct 12, 2013
 *      Author: sven
 */

#include <regex.h>
#include <set>
#include <guli/gffParser.h>
#include "reduceGffMode.h"
#include "GenomeAccessibilityModel.h"
#include "GeneEnrichmentAnalysis.h"
using namespace GULI;

reduceGffMode::reduceGffMode() :
		uiOperationMode("reducegff"),
	 	_overwrite("overwrite", "Overwrite output file if it exists",  _parms)
	{


	}
	// TODO Auto-generated constructor stub
//const string feature_regex = "(gene)|(transcript)|(CDS)|(exon)";
const string feature_regex = "(gene)|(transcript)|(CDS)|(exon)";

typedef struct geneRules {
	const char *attribute;
	const char  *regex;
	regex_t     _regex;
} geneRulesT;

static geneRulesT rulez[] =  {
		{ 0 }
};
//static geneRulesT rulez[] =  {
//		{"Type", "(lincRNA)|(protein_coding)|(miRNA)|(snoRNA)" },
//		{"Status", "KNOWN" },
//		{ 0 }
//};

static int compileRegex() {
	for(geneRulesT *r = rulez; r->attribute; ++r) {
		if(regcomp(&r->_regex, r->regex, REG_EXTENDED)) {
			return -1;
		}
	}
	return 0;
}


static std::set<string> keepMe;

ostream &operator<<(ostream &O, GULI::gffDag::Node &N) {
	O << N.chromosome() << "\t" << N.source() << "\t" << N.feature() << "\t"
      << N.start() << "\t" << N.end() <<  "\t" << N.score() << "\t" << N.orient() << "\t"
	  << N.frame() << "\t" << "ID=" << N.id();

	bool labeled = false;
	for( size_t i=0; i < N.nIncoming(); ++i) {
		if(N.incoming(i)->id() != gffDag::rootId) {
			if(!keepMe.count(N.incoming(i)->id())) continue;
			if(!labeled) {
				labeled = true;
				O << ";Parent=" << N.incoming(i)->id();
				continue;
			} else {
				O << ',' << N.incoming(i)->id();
			}
		}
	}

	vector<string>  A = N.knownAttributes();
	for(size_t i=0; i < A.size(); ++i)
		O << ";" << A[i] << "=" << N.attribute(A[i]);
	return O;
}

int reduceGffMode::main(int argc, char **argv) {
	regex_t regex;
	regmatch_t match;

	if(regcomp(&regex, feature_regex.c_str(), REG_EXTENDED) || compileRegex()) {
		std::cerr << "Error setting up regular expression " << feature_regex << std::endl;
		exit(-1);
	}

//	GenomeAccessibilityModel acc;
	Genome2Feature g2f("exon", &GULI::gffParser::Gencode);
	g2f.readGff(argv[1]);

	GULI::gffDag &gff(g2f.genomeAnnotation());
	Genome2Feature::featureListT genes;
	g2f.allFeatures(genes, "gene");

	for(size_t i=0; i < genes.size(); ++i) {
		GULI::gffDag::Node *G = genes[i];

		bool gene_acceptable = true;
		for(geneRulesT *r = rulez; r->attribute; ++r) {
			string value = G->attribute(r->attribute);
			if(value=="") {
				std::cerr << "Attribute " << r->attribute << " not found in " << G->id() << std::endl;
				continue;
			}
			if(regexec(&r->_regex, value.c_str(), 1, &match, 0)) {
				gene_acceptable = false;
				break;
			}
		}
		if(!gene_acceptable) continue;

 		for(gffDag::uniqueForwardIterator g(genes[i]); g !=gff.end(); ++g) {
			if(!regexec(&regex, g->feature().c_str(), 1, &match, 0)) {
				if(!keepMe.count(g->id())) keepMe.insert(g->id());
			}
		}
	}

	for(GULI::gffDag::uniqueForwardIterator U(gff.root());  ++U != gff.end(); ) {
	  if(keepMe.count(U->id()))  std::cout << *U << "\n";
	 }
	return 0;
}

static reduceGffMode _thatsme;
