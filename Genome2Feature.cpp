/*
 * Genome2Gene.cpp
 *
 *  Created on: Oct 8, 2013
 *      Author: sven
 */
#include <set>
#include <limits>
#include <guli/gffParser.h>
#include <guli/gffAlgo.h>
#include "Genome2Feature.h"
using namespace GULI;
using GULI::gffDag;
using GULI::bedRegion;
using GULI::bedTiling;

Genome2Feature::Genome2Feature(const char *probeFeatureRegexp, const GULI::gffParser::parserMode *M) :
	_cDNA(0),
	_probeFeatureRegexp(probeFeatureRegexp),
	_parserMode(M),
	_cDNAmagic(0)
{}

Genome2Feature::~Genome2Feature() {
	if(_cDNA) delete _cDNA;
}

void Genome2Feature::clear() {
	_annot.clear();
	_probeList.clear();
	if(_cDNA) delete _cDNA;
	_cDNA = 0;
}

int Genome2Feature::readGff(const char *fn) {
	clear();

	gffParser P;
	ifstream gffstream(fn);
	if(!gffstream) {
		std::cerr << "Can not read GFF definition " << fn << std::endl;
		return -1;
	}
	int r = P.parse(gffstream, _annot, *_parserMode).bad() ? -1  : 0;
	gffstream.close();
	if(r) return r;

	GULI::gffFindFeatures(GULI::GFF_DOWN, _probeList, _annot.root(), _probeFeatureRegexp);
	_cDNA = new constGffNodeListTiling(_probeList.begin(), _probeList.end());
	return 0;
}

void Genome2Feature::findProbes(const bedRegion &query, gffNodeListT &r) const {
	GULI::constGffNodeListTiling::targetListT probes;
	_cDNA->overlaps(query, probes);
	r.resize(probes.size());
	for(size_t i=0; i < probes.size(); ++i) {
		r[i] = _probeList[probes[i]];
	}
}

void Genome2Feature::findFeatures(const bedRegion &query,  gffNodeListT &r, const char *featureTypeRegex) const {
	gffNodeListT probes;
	findProbes(query, probes);
	gffFindFeatures(GFF_UP, r, probes.begin(), probes.end(), featureTypeRegex);
	return;
}

void Genome2Feature::cDNA(const featureListT &feature, bedRegion::listT &r) const {
	GULI::constGffNodeListTiling::targetListT index;
	cDNA(feature, index);
	r.resize(index.size());
	for(size_t i=0; i < index.size(); ++i) {
		r[i] = _cDNA->getTiling()[index[i]];
	}
}

size_t Genome2Feature::_newMagicNumber() const {
	if(++_cDNAmagic > std::numeric_limits<std::size_t>::max() - 100) {
		_cDNAmagic = 1;
		std::fill(_cDNAseen.begin(), _cDNAseen.end(), 0);
	}
	return _cDNAmagic;
}

void Genome2Feature::cDNA(const featureListT &feature, GULI::constGffNodeListTiling::targetListT &r) const {
	if(_cDNA->getTiling().size() != _cDNAseen.size()) {
		_cDNAseen.resize(_cDNA->getTiling().size());
		_cDNAmagic = std::numeric_limits<std::size_t>::max() - 2;
	}
	r.clear();
	size_t magic = _newMagicNumber();

	gffNodeListT probes;
	gffFindFeatures(GFF_DOWN, probes, feature.begin(), feature.end(),  _probeFeatureRegexp);
	for(size_t i=0; i < probes.size(); ++i) {
		GULI::constGffNodeListTiling::targetListT indices;
		_cDNA->overlapingTiles(*probes[i], indices);
		for(size_t j=0; j < indices.size(); ++j) {
			if(_cDNAseen[indices[j]] == magic) continue;
			_cDNAseen[indices[j]] = magic;
			r.push_back(indices[j]);
		}
	}
}

void Genome2Feature::allFeatures(featureListT &r, const char *featureTypeRegex)  {
	gffFindFeatures(GFF_DOWN, r, genomeAnnotation().root(), featureTypeRegex);
	return;
}

