/*
 * reduceGffMode.h
 *
 *  Created on: Oct 12, 2013
 *      Author: sven
 */

#ifndef REDUCEGFFMODE_H_
#define REDUCEGFFMODE_H_

#include <guli/uiOperationMode.h>
#include <guli/uiParms.h>
#include <guli/bedTiling.h>
#include <guli/gffDag.h>
#include <guli/gffAlgo.h>
class reduceGffMode: public GULI::uiOperationMode {
public:
public:
	reduceGffMode();
	virtual ~reduceGffMode(){};
	virtual int main(int argc, char **argv);

protected:
	GULI::uiParms      _parms;
	touchVarT          _overwrite;
	GULI::gffDag       _geneModel;
	GULI::gffNodeListT _cdna;

private:
	int _getGeneModel(const char *gffFileName, const GULI::gffParser::parserMode &semantic);
};

#endif /* REDUCEGFFMODE_H_ */
