/*
 * snvOntologyMain.cc
 *
 *  Created on: Oct 5, 2013
 *      Author: sven
 */

#include <sys/stat.h>
#include <fstream>
#include <guli/gffParser.h>
#include "snvOntologyMain.h"
#include "GoEnrichmentAnalysis.h"
#include "GeneEnrichmentAnalysis.h"
#include "keggEnrichmentAnalysis.h"
#include "Genome2Feature.h"
#include "GenomeAccessibilityModel.h"

using namespace GULI;
class DatasetDescriptor {};



class inputParser {
public:
	inputParser(EnrichmentAnalysis &analysis) :   _analysis(analysis) {};
	virtual ~inputParser() {}
	virtual istream &readData(istream &) = 0;
protected:
	EnrichmentAnalysis &analysis() { return _analysis; }
private:
	EnrichmentAnalysis &_analysis;
};


class bedParser : public inputParser {
public:
	bedParser(EnrichmentAnalysis &analysis) :  inputParser(analysis) {};
	virtual ~bedParser(){}
	virtual istream &readData(istream &IN) {
		bedRegion::listT D;
		if(bedRegion::readList(D, IN)) {
			IN.setstate(std::ios::badbit);
		} else {
			for(const bedRegion &b : D) {
				analysis().event(b);
			}
		}
		return IN;
	}
};


class vcfParser : public inputParser, public tabDelimited {
public:
	vcfParser(EnrichmentAnalysis &analysis) :  inputParser(analysis) {};
	virtual ~vcfParser(){};

protected:
	virtual istream &readData(istream &IN) { return parse(IN); }
	virtual void consumeData(int argc, char **argv) {
		if(argc < 7) return;
		if(strcmp("PASS", argv[6])) return;
		if(strlen(argv[2]) > 2 ) return;
		//		if(strcmp("REF_HET", argv[6])) return;
		string chr = argv[0];
		int    pos = atoi(argv[1]);
		bedRegion B(chr, "", pos, pos+1);
		analysis().event(B);
	}
};

class fusionParser : public inputParser,  public tabDelimited  {
public:
	fusionParser(EnrichmentAnalysis &analysis) :  inputParser(analysis), _lastSample(""), _source(0) {
	};
	virtual ~fusionParser(){}

private:
	virtual istream &readData(istream &IN) { return parse(IN); }
	virtual void consumeData(int argc, char **argv) {
		if(argc < 15) return;
		if(_lastSample != argv[0]) {
			if(_lastSample != "") {
				analysis().endSource();
				analysis().beginSource(++_source);
			}
			_lastSample = argv[0];
		}
		if(1 || (strcmp(argv[10], "ADJACENT") && strcmp(argv[10], "INTERGENIC")) ) {
			string chr = argv[1];
			int    pos1 = atoi(argv[2]);
			int    pos2 = atoi(argv[3]);
			bedRegion B(chr, "", pos1, pos2+1);
			analysis().event(B);
		}

		if(1 || (strcmp(argv[14], "ADJACENT") && strcmp(argv[14], "INTERGENIC")) ) {
			string chr = argv[8];
			int    pos1 = atoi(argv[9]);
			int    pos2 = atoi(argv[10]);
			bedRegion B(chr, "", pos1, pos2+1);
			analysis().event(B);
		}
	}

	string _lastSample;
	int    _source;
};

const snvOntologyMain::gffSyntaxT::enumvarcfg snvOntologyMain::dialectChoice[] = {
		{"ensembl", &GULI::gffParser::Ensembl },
		{"gencode", &GULI::gffParser::Gencode },
		{"native",  &GULI::gffParser::Native },
		{""}
};

const snvOntologyMain::modeT::enumvarcfg snvOntologyMain::modeChoice[] = {
		{"fusion",  FUSION},
		{"snv",     SNV },
		{"bed",     BED },
		{""}
};


snvOntologyMain::snvOntologyMain() :
	uiOperationMode("ontology"),
 	_overwrite("overwrite", "Overwrite output file if it exists",  _parms),
	_semantic("dialect", "Select GFF dialect: ensembl, gencode, native", dialectChoice, &GULI::gffParser::Gencode, _parms),
	_mode("mode", "select mode: fusion or snv", modeChoice, 1, _parms),
	_maxpval("pvalcut", "Max Pvalue to report", 1., _parms)
{}

int snvOntologyMain::main(int argc, char **argv) {
	int r = _parms.commandLine(argc, argv);
	argc -= r;
	argv += r;
	if((r < 0) or (argc < 3)) {
		std::cerr << "Usage: " << argv[0] << " [options] gffdef.gff bamfile [bamfile ...] outFile\n" << std::endl;
		std::cerr << "Known options are" << std::endl;
		std::cerr << _parms.describeParms() << std::endl;
		return -1;
	}


	struct stat dummy;
	if(!(_overwrite ||  stat(argv[argc-1], &dummy))) {
		std::cerr << "Output file " << argv[argc-1] << " already exists. Cowardly refusing to overwrite. Use -overwrite to convince me!" << std::endl;
		return -1;
	}
	ofstream OUT(argv[argc-1]);
	if(!OUT) {
		std::cerr << "Can not open output file " << argv[argc-1] << std::endl;
		return -1;
	}

	GenomeAccessibilityModel acc;
	DatasetDescriptor D;
//	Genome2Feature g2f(_mode == SNV ? "exon" : "transcript", &GULI::gffParser::Gencode);
	Genome2Feature g2f(_mode == SNV ? "exon" : "transcript", &GULI::gffParser::Native);
	const char *goFile = "/tmp/go.xml";
	const char *goAssocFile = "/tmp/goAssoc.csv";
//	const char *goAssocFile = 0;
	const char *keggFile = "/tmp/kegg.xml";
	const char *keggAssocFile = "/tmp/goAssoc.csv";
	g2f.readGff(argv[1]);
//	EnrichmentAnalysis *analysis = new GoEnrichmentAnalysis(g2f, acc, goFile, goAssocFile);
//	EnrichmentAnalysis *analysis = new keggEnrichmentAnalysis(g2f, acc, keggFile, keggAssocFile);
	EnrichmentAnalysis *analysis = new GeneEnrichmentAnalysis(g2f, acc);

	if(analysis->init()) {
		std::cerr << "Error during initialization of the analysis object" << std::endl;
		exit(-1);
	}
	analysis->regionHits(true);
	analysis->prepareAnalysis(D);

	inputParser &input = (_mode == SNV) ? *static_cast<inputParser *>(new vcfParser(*analysis)) :
			             (_mode == BED) ? *static_cast<inputParser *>(new bedParser(*analysis)) :
			            		          *static_cast<inputParser *>(new fusionParser(*analysis));

	for(int i=2; i < argc-1; ++i) {
		analysis->beginSource(i-2);
		ifstream IN(argv[i]);
		if(!IN) {
			std::cerr << "Could not open " << argv[i] << std::endl;
			continue;
		}

		input.readData(IN);
		IN.close();
		analysis->endSource();
	}
	analysis->endAnalysis();

	const size_t nCat = analysis->nCategories();
	OUT << "ID\tHITS\tPVAL\tENRICH\tBASES\tDescritption\n";
	for(size_t i=0; i < nCat; ++i) {
		if(!analysis->categoryHits(i)) continue;
		if(analysis->categoryPValue(i) > _maxpval) continue;
		OUT << analysis->categoryId(i) << "\t";
		OUT << analysis->categoryHits(i) << "\t";
		OUT << analysis->categoryPValue(i) << "\t";
		OUT << analysis->categoryEnrichment(i) << "\t";
		OUT << analysis->categoryBases(i) << "\t";
		OUT << analysis->categoryDescription(i) << "\n";
	}

	delete &input;
	delete analysis;
	return 0;
}

static snvOntologyMain _myself;
