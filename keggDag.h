/*
 * keggDag.h
 *
 *  Created on: Oct 18, 2013
 *      Author: sven
 */

#ifndef KEGGDAG_H_
#define KEGGDAG_H_

#include <guli/dag.h>
#include <guli/dagIterator.h>
#include <guli/utilAttributeFunctionality.h>
//#include "utilHash.h"

namespace GULI {
	/** The definition of a (binary) GFF Node
	 **/

	class keggDag : public Dag {
	public:
		class Node;
		typedef dagTemplates::forwardIterator<Node>  forwardIterator;
		typedef dagTemplates::backwardIterator<Node> backwardIterator;
		typedef dagTemplates::uniqueForwardIterator<Node>  uniqueForwardIterator;
		typedef dagTemplates::uniqueBackwardIterator<Node> uniqueBackwardIterator;

		typedef struct linkTypeS {
			size_t 	IS_A:1,
					PART_OF:1,
					AGENT:1,
					INPUT:1,
					OUTPUT:1,
					INHIBITOR:1;
		} linkTypeBitfieldT;

		typedef union linkTypeU {
			typedef void *voidVar;
			voidVar voidRepresentation;
			linkTypeBitfieldT bitfield;
			linkTypeU() { this->voidRepresentation = 0; }
			operator voidVar&() { return voidRepresentation; };
			operator linkTypeBitfieldT&() { return bitfield; };
		} linkTypeT;


		virtual ~keggDag();
		void clear();

		Node *operator[]   (const string &id) const;
		static Node *end()  						{ return (Node *) -1; }
		Node *root() const 							{ return this->operator[](rootId); }
		void addNode(Node *N);
		static const string rootId, orphanId;
	private:
	};


	class keggDag::Node : public Dag::Node, public utilAttributeFunctionality {
	public:
		typedef enum {MOLECULE, MOLECULE_LIST,  INTERACTION, PATHWAY, TECHNICAL} nodeTypeT;
		static const string childAttribute, listType, listTypeFamily, listTypeComplex;

		class textual : public utilAttributeFunctionality{
		public:
			textual(const string ID, nodeTypeT TYPE) : type(TYPE), id(ID){}
			nodeTypeT   type;
			string 		id;
			vector<string> childIds;
		};
		typedef vector<textual> table;

		Node(const textual &T);
		Node(const string &id, nodeTypeT type) : _id(id), _type(type) {}

		virtual const string &id()    				const { return _id; }
		nodeTypeT type()  						const { return _type; }

		ostream &operator<<(ostream &o) const;
	private:
		string  _id;
		nodeTypeT _type;
	};
}	 // NAMESPACE



#endif /* KEGGDAG_H_ */
