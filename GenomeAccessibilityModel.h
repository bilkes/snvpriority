/*
 * GenomeAccessibilityModel.h
 *
 *  Created on: Oct 9, 2013
 *      Author: sven
 */

#ifndef GENOMEACCESSIBILITYMODEL_H_
#define GENOMEACCESSIBILITYMODEL_H_
#include "Genome2Feature.h"
class DatasetDescriptor;

class GenomeAccessibilityModel {
public:
	GenomeAccessibilityModel(){};
	virtual ~GenomeAccessibilityModel(){};

	virtual void init(const Genome2Feature &F, const DatasetDescriptor &D) {}
	virtual bool operator()(const size_t cdna_index) const                          { return true; }
	virtual bool operator()(const size_t cdna_index, const size_t file_index) const { return true; }
};

#endif /* GENOMEACCESSIBILITYMODEL_H_ */
