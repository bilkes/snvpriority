//============================================================================
// Name        : main.cpp
// Author      : Sven Bilke
// Version     :
// Copyright   : GPL V3.0
// Description : Hello World in C++, Ansi-style
//============================================================================
using namespace std;
#include <iostream>
#include <guli/uiOperationMode.h>
#include "version.h"
/** Welcome, Stranger. If you wonder where to start reading (or, how does main() know about
 *  all the nice (?) things it can do), have a look at *Mode.cc files. Instantiating objects derived
 *  from uiOperatioMode will (generally) be made available to the "entryPoint" method.
 *
 *  The user interface (-option ....) is built dynamically when  objects defined in uiparms.*,
 *  namely  uiparms::Variable<>, are instantiated.
 *
 */


int main(int argc, char **argv) {
	return GULI::uiOperationMode::entryPoint(argc, argv);
}



static GULI::uiHelpMode    _help;
static GULI::uiVersionMode _version(__VERSIONSTRING);
