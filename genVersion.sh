#!/bin/bash
VER=`git log --decorate | head -3 | sed -e 's/commit//' | 
  perl -e 'my $lc = 0; while(<>) { chomp; print ", " if($lc++); print $_;}'`;
echo "#define __VERSIONSTRING \"$VER\"";

