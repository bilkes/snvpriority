/*
 * keggEnrichmentAnalysis.cpp
 *
 *  Created on: Oct 22, 2013
 *      Author: sven
 */

#include <sstream>
#include <guli/tabDelimited.h>
#include "keggDagXmlIo.h"
#include "keggEnrichmentAnalysis.h"

// location of the kegg-map file for ensembl http://useast.ensembl.org/biomart/martview

using namespace GULI;


keggEnrichmentAnalysis::keggEnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A, const char *keggDef, const char *keggAssoc)
   : EnrichmentAnalysis(F,A),
  _keggDefintionFile(keggDef),
  _keggAssocFile(keggAssoc)
{ }

keggEnrichmentAnalysis::~keggEnrichmentAnalysis() {}


class keggAssocParser: public GULI::tabDelimited {
public:
	keggAssocParser(keggDag &kegg, gffDag &ensemble)
	: _kegg(kegg),
	  _ensemble(ensemble),
	  _quiet(false) {}

protected:
	void consumeData(int argc, char **argv) {
		if(argc != 2) return;
		string ensembleId = argv[0];
		string keggId      = argv[1];

		if(! (_kegg.exists(keggId) && _ensemble.exists(ensembleId)) ) {
			if(!_quiet)  {
				if (!_kegg.exists(keggId)) {
					std::cerr << "WARNING: kegg " << keggId << " referenced in the assoc file is unknown" << std::endl;
				} else {
					std::cerr << "WARNING: ensemble item  " << ensembleId << " referenced in the assoc file is unknown" << std::endl;
				}
			}
			return;
		}

		keggDag::Node *goterm       = _kegg[keggId];
		gffDag::Node  *feature      = _ensemble[ensembleId];

		for(keggDag::uniqueBackwardIterator i(goterm); i != _kegg.root(); ++i) {
			keggEnrichmentAnalysis::keggPayload &P(*static_cast<keggEnrichmentAnalysis::keggPayload *>(i->getUserVariable()));
			P.addAnnotSingle(feature);
		}
		return;
	}
private:
	keggDag      &_kegg;
	gffDag       &_ensemble;
	bool         _quiet;
};

int keggEnrichmentAnalysis::_init() {
	keggDagXmlIO IN;
	if(IN.read(_keggDefintionFile, _kegg)) {
		std::cerr << "Can not parse kegg definition file " << _keggDefintionFile << std::endl;
		return -1;
	}

	// Decorate KEGG with associations
	const size_t nKegg = _kegg.size();
	_keggPayloadBuffer.resize(nKegg);
	size_t idx = 0;
	for(keggDag::uniqueForwardIterator I(_kegg.root()); I != _kegg.end(); ++I) {
		_keggPayloadBuffer[idx].clearAnnot();
		_keggPayloadBuffer[idx].index(idx);
		I->setUserVariable((void *) &_keggPayloadBuffer[idx++]);
	}

	for(size_t i=0; i < _knownFeatures().size(); ++i) {
		static_cast<FeaturePayload *>(_knownFeatures()[i]->getUserVariable())->clearKegg();
	}

	keggAssocParser P(_kegg, _g2f().genomeAnnotation());
	ifstream associationFile(_keggAssocFile);
	if(!associationFile) {
		std::cerr << "Can not open KEGG association file " << _keggAssocFile << std::endl;
		return -1;
	}
	P.parse(associationFile);
	int r = associationFile.bad() ? -1 : 0;
	associationFile.close();
	if(r) return r;

	_categoryIds.clear();
	_categoryIds.reserve(_kegg.size());
	for(keggDag::uniqueForwardIterator kegg(_kegg.root()); kegg != _kegg.end(); ++kegg) {
		if(kegg->type() != keggDag::Node::PATHWAY) continue;
		_categoryIds.push_back(kegg->id());
		keggPayload *KEP = static_cast<keggPayload  *> (kegg->getUserVariable());
		if(!KEP) continue;
		keggPayload::annotListT &A(KEP->targetList());
		for(keggPayload::annotListT::iterator i = A.begin(); i != A.end(); ++i) {
			gffDag::Node *N = *i;
			FeaturePayload *FP = static_cast<FeaturePayload *>(N->getUserVariable());
			FP->addKegg(&(*kegg));
		}
	}
	return 0;
}

void keggEnrichmentAnalysis::_allocateFeaturePayloads(size_t s) {
	_featurePayloadBuffer.clear();
	_featurePayloadBuffer.resize(s);
}

void keggEnrichmentAnalysis::categoryFeatures(size_t category, Genome2Feature::featureListT &result) {
	keggPayload *P = static_cast<keggPayload *>(_kegg[categoryId(category)]->getUserVariable());
	result.resize(P->annot().size());
	std::copy(P->annot().begin(), P->annot().end(), result.begin());
}

void keggEnrichmentAnalysis::featureCategories(Genome2Feature::featureT &F,  categoryListT &result) {
	FeaturePayload *P = static_cast<FeaturePayload *>(F->getUserVariable());
	const size_t N = P->targetList().size();
	result.resize(N);
	size_t idx=0;
	for(FeaturePayload::annotListT::iterator i=P->targetList().begin(); i != P->targetList().end(); ++i) {
		result[idx++] = static_cast<keggPayload *>((*i)->getUserVariable())->index();
	}
}

string  keggEnrichmentAnalysis::categoryDescription(size_t category) { return "NOT IMPLEMENTED"; }

/*
string  keggEnrichmentAnalysis::categoryDescription(size_t category) {
	string r = _kegg[categoryId(category)]->name() + "\t";
	Genome2Feature::featureListT features;
	categoryFeatures(category, features);
	bool first = true;
	for(size_t i=0; i < features.size(); ++i) {
		FeaturePayload *P = static_cast<FeaturePayload *>(features[i]->getUserVariable());
		if(P->totalCount()) {
			if(!first) r += ",";
			r += features[i]->attribute("Name");
			r += "(";
			std::stringstream count;
			count << P->totalCount();
			r += count.str();
			r += ")";
			first = false;
		}
	}
	r += "\t" + _go[categoryId(category)]->description();
	return r;
}
*/
