/*
 * keggDagXmlIo.h
 *
 *  Created on: Oct 18, 2013
 *      Author: sven
 */

#ifndef KEGGDAGXMLIO_H_
#define KEGGDAGXMLIO_H_
#include "keggDag.h"
#include <unordered_map>
//#include <guli/utilHash.h>
#include <guli/xmlGeneric.h>

namespace GULI {
	class keggDagXmlIO : public xmlGeneric {
		public:
			int read(const char *filename, keggDag &target);
			keggDagXmlIO();
			virtual ~keggDagXmlIO();

		protected:
			void _textual2Dag(keggDag &target);

		private:
			virtual bool _eatToken(tokentype &token);
			typedef keggDag::Node::textual (keggDagXmlIO::*processM)(xmlGeneric::tokentype &token);

//			typedef __guliHash<string, processM, UTIL::stringHashFunc> _processorRulesT;
			typedef std::unordered_map<string, processM> _processorRulesT;
			typedef std::pair<string, processM> _ruleInitT;

			keggDag::Node::textual _molecule(xmlGeneric::tokentype &token);
			keggDag::Node::textual _interaction(xmlGeneric::tokentype &token);
			keggDag::Node::textual _pathway(xmlGeneric::tokentype &token);
			keggDag::Node::textual _ignore(xmlGeneric::tokentype &token);

			static const _ruleInitT _ruleInit[];
			_processorRulesT _rulez;

			keggDag::Node::table _textualTable;
			bool _quiet;

	};
} //NAMESPACE


#endif /* KEGGDAGXMLIO_H_ */
