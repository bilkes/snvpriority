/*
 * keggDagXmlIo.cpp
 *
 *  Created on: Oct 18, 2013
 *      Author: sven
 */
#include <string.h>
#include "guli/stdinc.h"

#include "keggDagXmlIo.h"

static const char *val_key    = "value";
static const char *id_key     = "id";

namespace GULI {
	const keggDagXmlIO::_ruleInitT keggDagXmlIO::_ruleInit[] = {
		{"Created",      		 &keggDagXmlIO::_ignore},
		{"Interaction",  		 &keggDagXmlIO::_interaction},
		{"Molecule",     		 &keggDagXmlIO::_molecule},
		{"Ontology",     		 &keggDagXmlIO::_ignore},
		{"Pathway",      		 &keggDagXmlIO::_pathway},
	};



	keggDagXmlIO::keggDagXmlIO()
	: _rulez(_ruleInit, _ruleInit + sizeof(_ruleInit) / sizeof (_ruleInitT)),
	  _quiet(false)
	{}

	keggDagXmlIO::~keggDagXmlIO() {}

	int keggDagXmlIO::read(const char *filename, keggDag &target) {
		if (!parse(filename)) {
			std::cerr << "Warning: error reading KEGG definition file" << std::endl;
			return -1;
		}
		target.clear();
		_textual2Dag(target);
		_textualTable.clear();
		return 0;
	}

	bool keggDagXmlIO::_eatToken(tokentype &token) {
		if(_rulez.find(token.name) == _rulez.end()) return false; // keep assembling
		keggDag::Node::textual t = (this->*_rulez[token.name])(token);
		if(t.type != keggDag::Node::TECHNICAL) 	_textualTable.push_back(t);
		if(t.type < 3) return true;
//		std::cerr << "DEBUG: " << t.id << "\t" << t.type;
	//	for(size_t i=0; i < t.knownAttributes().size(); ++i)
	//		std::cerr << "\t" << t.knownAttributes()[i] << ":" << t.getAttribute(t.knownAttributes()[i]);
	//	std::cerr << std::endl;
		return true;
	}

	keggDag::Node::textual keggDagXmlIO::_ignore(xmlGeneric::tokentype &token) {
		keggDag::Node::textual r("IGNORE_ME", keggDag::Node::TECHNICAL);
		return r;
	}

	keggDag::Node::textual keggDagXmlIO::_molecule(xmlGeneric::tokentype &token) {
		static const char *mtype_key  		= "molecule_type";
		static const char *lntype_key 		= "long_name_type";
		static const char *fml_key    		= "FamilyMemberList";
		static const char *member_key 		= "Member";
		static const char *idref_key  		= "member_molecule_idref";
		static const char *cmp_key    		= "ComplexComponentList";
		static const char *component_key 	= "ComplexComponent";
		static const char *molIdref_key  	= "molecule_idref";
		static const char *name_key   		= "Name";

		string id    = findAttribute(token, id_key);
		keggDag::Node::textual r(id, keggDag::Node::MOLECULE);
		string type  = findAttribute(token, mtype_key);
		if(type != utilAttributeFunctionality::UNKNOWNATTRIBUTE) r.addReplaceAttribute(mtype_key, type);

		xmlGeneric::tokentype name;
		for(startRepFindToken(token, name_key); findNextToken(name); ) {
			string name_type = findAttribute(name, lntype_key);
			string value     = findAttribute(name, val_key);
			string oldValue = r.attribute(name_type);
			string newValue = (oldValue != utilAttributeFunctionality::UNKNOWNATTRIBUTE) ? oldValue + "," : "";
			newValue += value;
			r.addReplaceAttribute(name_type, newValue);
		}

		xmlGeneric::tokentype family;
		if(findToken(token, fml_key, family)) {
			r.type = keggDag::Node::MOLECULE_LIST;
			string child;
			xmlGeneric::tokentype member;
			for(startRepFindToken(family, member_key); findNextToken(member); ) {
				if(child.length()) child += ",";
				child += findAttribute(member, idref_key);
			}
			r.addReplaceAttribute(keggDag::Node::childAttribute, child);
			r.addReplaceAttribute(keggDag::Node::listType, keggDag::Node::listTypeFamily);
			return r;
		}

		xmlGeneric::tokentype complex;
		if(findToken(token, cmp_key, complex)) {
			r.type = keggDag::Node::MOLECULE_LIST;
			string child;
			xmlGeneric::tokentype member;
			for(startRepFindToken(complex, component_key); findNextToken(member); ) {
				if(child.length()) child += ",";
				child += findAttribute(member, molIdref_key);
			}
			r.addReplaceAttribute(keggDag::Node::childAttribute, child);
			r.addReplaceAttribute(keggDag::Node::listType, keggDag::Node::listTypeComplex);
			return r;
		}

		return r;
	}

	keggDag::Node::textual keggDagXmlIO::_interaction(xmlGeneric::tokentype &token) {
		static const char *itype_key 			= "interaction_type";
		static const char *componentList_key 	= "InteractionComponentList";
		static const char *component_key 		= "InteractionComponent";
		static const char *role_key 			= "role_type";
		static const char *reference_key 		= "molecule_idref";

		string id    = findAttribute(token, id_key);
		keggDag::Node::textual r(id, keggDag::Node::INTERACTION);
		string type  = findAttribute(token, itype_key);
		if(type !=  xmlGeneric::UNKNOWNATTRIBUTE) r.addReplaceAttribute("type", type);

		xmlGeneric::tokentype component, componentList;
//		typedef __guliHash<string, string, UTIL::stringHashFunc> roleT;
		typedef std::unordered_map<string, string> roleT;
		roleT rolez;

		if(findToken(token, componentList_key, componentList)) {
			for(startRepFindToken(componentList, component_key); findNextToken(component); ) {
				string role_type  = findAttribute(component, role_key);
				string molecule   = findAttribute(component,  reference_key);
				string roles 	  = rolez[molecule];
				if(roles.length()) roles += "#";
				roles += role_type;
				rolez[molecule] = roles;
			}
		}
		string childString;
		for(roleT::iterator i=rolez.begin(); i != rolez.end(); ++i) {
			const string separator = ":";
			string molecule = i->first;
			string rolez    = i->second;
			if(childString.length()) childString += ",";
			childString += molecule + separator + rolez;
		}
		r.addReplaceAttribute(keggDag::Node::childAttribute, childString);

		return r;
	}

	keggDag::Node::textual keggDagXmlIO::_pathway(xmlGeneric::tokentype &token) {
		string id    = findAttribute(token, "id");
		keggDag::Node::textual r(id, keggDag::Node::PATHWAY);
		string subnet  = findAttribute(token, "subnet");
		if(subnet !=  xmlGeneric::UNKNOWNATTRIBUTE) r.addReplaceAttribute("subnet", subnet);

		xmlGeneric::tokentype tmp;
		if(findToken(token, "Organism", tmp))  r.addReplaceAttribute("organism",   tmp.text);
		if(findToken(token, "LongName", tmp))  r.addReplaceAttribute("name",       tmp.text);
		if(findToken(token, "ShortName", tmp)) r.addReplaceAttribute("shortName", tmp.text);


		xmlGeneric::tokentype component;
		if(findToken(token, "PathwayComponentList", tmp)) {
			for(startRepFindToken(tmp, "PathwayComponent"); findNextToken(component); ) {
				string value     = findAttribute(component,  "interaction_idref");
				string oldValue  = r.attribute(keggDag::Node::childAttribute);
				oldValue         = oldValue == utilAttributeFunctionality::UNKNOWNATTRIBUTE ? "" : oldValue + ",";
				string newValue  = oldValue + value;
				r.addReplaceAttribute(keggDag::Node::childAttribute, newValue);
			}
		}
		return r;
	}

	void keggDagXmlIO::_textual2Dag(keggDag &target) {
		keggDag::linkTypeT PARTOF; PARTOF.bitfield.PART_OF = 1;
		for (size_t i = 0; i < _textualTable.size(); i++) {
			const keggDag::Node::textual &cur = _textualTable[i];
			if(target.exists(cur.id)) {
				std::cerr << "WARNING: KEGG item " << cur.id << " defined more than once." << std::endl;
				continue;
			}
			target.addNode(new keggDag::Node(cur));
		}

		// fill in links
		for (unsigned int i = 0; i < _textualTable.size(); i++) {
			const keggDag::Node::textual &curt = _textualTable[i];
			keggDag::Node &curn = *target[curt.id];

			switch(curt.type) {
				case keggDag::Node::TECHNICAL:
				case keggDag::Node::MOLECULE:
					break;

				case keggDag::Node::PATHWAY:
				case keggDag::Node::MOLECULE_LIST:
				{
					if(curt.type == keggDag::Node::PATHWAY) {
						keggDag::Node &root = *target[keggDag::rootId];
						Dag::Link link2root(root, Dag::Link::INCOMING,  (Dag::Link::userdataT) PARTOF);
						Dag::Link linkFroot(curn, Dag::Link::OUTGOING,  (Dag::Link::userdataT) PARTOF);
						curn.addLink(link2root);
						root.addLink(linkFroot);
					}
					vector<string> interactions;
					string i = curt.attribute(keggDag::Node::childAttribute);
					if(i == utilAttributeFunctionality::UNKNOWNATTRIBUTE) break;
					while(i.length()) {
						size_t pos = i.find_first_of(',');
						interactions.push_back(i.substr(0, pos));
						if(pos != string::npos) i = i.substr(pos+1); else i="";
					}
					if(interactions.size()) {
						target.updateRelations(curn, interactions,  Dag::Link::OUTGOING, PARTOF);
					}
					break;
				}

				case keggDag::Node::INTERACTION: {
					string i = curt.attribute(keggDag::Node::childAttribute);
					if(i == utilAttributeFunctionality::UNKNOWNATTRIBUTE) break;
					while(i.length()) {
						size_t pos = i.find_first_of(',');
						string cur = i.substr(0, pos);
						if(pos != string::npos) i = i.substr(pos+1); else i="";

						pos = cur.find_first_of(':');
						string moleculeId = cur.substr(0,pos);
						if(!target.exists(moleculeId)) {
							std::cerr << "Reference to unknown molecule " << moleculeId <<
									     " while setting up interaction " << curt.id << std::endl;
							continue;
						}
						if(pos == string::npos) {
							std::cerr << "Referenced  molecule " << moleculeId <<
									     " without role-type annotation " << curt.id << std::endl;
							continue;

						}

						keggDag::Node &partner = *target[moleculeId];
						// setup molecule roles
						cur = cur.substr(pos+1);
						keggDag::linkTypeT role;
						while(cur.length()) {
							pos = cur.find_first_of('#');
							string r = cur.substr(0,pos);
							if(pos != string::npos) cur = cur.substr(pos+1); else cur="";
							if(r=="agent") role.bitfield.AGENT=1;
							else if(r=="input") role.bitfield.INPUT=1;
							else if(r=="output") role.bitfield.OUTPUT=1;
							else if(r=="inhibitor") role.bitfield.INHIBITOR=1;
							else std::cerr << "Unrecognized interaction role " << r << std::endl;
						}
						curn.addLink(Dag::Link(partner, Dag::Link::OUTGOING,  role));
						partner.addLink(Dag::Link(curn, Dag::Link::INCOMING,  role));
					}
					break;
				}
			} // switch
		} // for(T elements)

		keggDag::Node &orphans = *target[keggDag::orphanId];

		for (size_t i = 0; i < _textualTable.size(); i++) {
			keggDag::Node &curn = *target[_textualTable[i].id];
			if(!curn.nIncoming()) {
				curn.addLink(Dag::Link(orphans, Dag::Link::INCOMING,  PARTOF));
				orphans.addLink(Dag::Link(curn, Dag::Link::OUTGOING,  PARTOF));
	//			std::cerr << "Orphan " << curn.id() << std::endl;
			}
		}



	} // method
} // namespace
