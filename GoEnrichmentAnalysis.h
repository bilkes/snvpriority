/*
 * GoEnrichmentAnalysis.h
 *
 *  Created on: Oct 9, 2013
 *      Author: sven
 */

#ifndef GOENRICHMENTANALYSIS_H_
#define GOENRICHMENTANALYSIS_H_
#include <set>
#include <guli/dagIterator.h>
#include <guli/geneOntology.h>
#include "EnrichmentAnalysis.h"

using GULI::Dag;
using GULI::geneOntology;


class GoEnrichmentAnalysis: public EnrichmentAnalysis {
public:
	GoEnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A, const char *godef, const char *goAssoc);
	virtual ~GoEnrichmentAnalysis();



	virtual size_t nCategories() const { return _go.size(); }
	virtual string  categoryId(size_t category) { return _goTermIds[category]; }
	virtual string  categoryDescription(size_t category);
	virtual void   categoryFeatures(size_t category, Genome2Feature::featureListT &result);
	virtual void   featureCategories(Genome2Feature::featureT &F,  categoryListT &result);


	geneOntology &ontology() { return _go; }

	class FeaturePayload : public EnrichmentAnalysis::featurePayload {
	public:
		typedef std::set<geneOntology::Node *> annotListT;
		virtual ~FeaturePayload() {}
		void addGo(geneOntology::Node *N) { _gos.insert(N); }
		annotListT &targetList() { return _gos; }
		void clearGo() { _gos.clear(); }
	private:
		annotListT _gos;
	};

	class GoPayload  {
	public:
		typedef std::set<GULI::gffDag::Node *> annotListT;
		virtual ~GoPayload() {}
		void addAnnotSingle(GULI::gffDag::Node *N) {_annot.insert(N); }
		annotListT &targetList() { return _annot; }
		void clearAnnot() { _annot.clear(); }
		size_t index()  const  { return _idx; }
		size_t index(size_t i) { return _idx = i; }
		annotListT &annot() { return _annot; }
	private:
		annotListT _annot;
		size_t _idx;
	};

	protected:
	virtual void _allocateFeaturePayloads(size_t s);
	virtual featurePayload *_featurePayloadObject(size_t i) { return &_featurePayloadBuffer[i]; }
	virtual int _init();

private:
	geneOntology _go;
	vector<GoPayload>      _goPayloadBuffer;
	vector<FeaturePayload> _featurePayloadBuffer;
	vector<string>           _goTermIds;
	const char              *_goDefintionFile;
	const char              *_goAssocFile;
};

#endif /* GOENRICHMENTANALYSIS_H_ */
