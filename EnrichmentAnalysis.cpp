/*
 * genericAnnotation.cpp
 *
 *  Created on: Oct 8, 2013
 *      Author: sven
 */

#include "EnrichmentAnalysis.h"
#include <guli/bedRegion.h>
#include <numeric>
#include <guli/numericDistribution.h>

using GULI::bedRegion;

static const size_t nSpecialCategories = 0;

EnrichmentAnalysis::EnrichmentAnalysis(Genome2Feature &f, GenomeAccessibilityModel &a) :
	_genome(f),
	_accessibility(a),
	_dataset(0),
	_currentFile(0),
	_totalBases(0),
	_totalHits(0),
	_totalEventSize(0),
	_multiHit(false)
{}

EnrichmentAnalysis::~EnrichmentAnalysis() {}

int EnrichmentAnalysis::init() {
	_genome.allFeatures(__knownFeatures, _annotatedFeatureRegex());
	_allocateFeaturePayloads(__knownFeatures.size());
	int payloadIndex = 0;
	for(unsigned int i=0; i < __knownFeatures.size(); ++i)
		__knownFeatures[i]->setUserVariable(_featurePayloadObject(payloadIndex++));

	return _init();
}

void EnrichmentAnalysis::prepareAnalysis(const DatasetDescriptor &D) {
	_dataset = &D;
	const int nCat  = nCategories();

	_bases.resize(nCat + nSpecialCategories);
	std::fill(_bases.begin(), _bases.end(), 0);

	_categoryHits.resize(nCat + nSpecialCategories);
	std::fill(_categoryHits.begin(), _categoryHits.end(), 0);

	_pvalue.resize(nCat + nSpecialCategories);
	std::fill(_pvalue.begin(), _pvalue.end(), 1.);

	_enrich.resize(nCat + nSpecialCategories);
	std::fill(_enrich.begin(), _enrich.end(), 0.);

	Genome2Feature::featureListT F;
	GULI::constGffNodeListTiling::targetListT indices;
	const GULI::constGffNodeListTiling &tiling(*_genome.cDNA());


	_totalHits  = _totalBases = _totalEventSize = 0;

	vector<bool> utilizedBefore(tiling.getTiling().size(), false);
	for(int i=0; i < nCat; ++i) {
		categoryFeatures(i, F);
		_genome.cDNA(F, indices);
		for(size_t j=0; j < indices.size(); ++j) {
			size_t idx = indices[j];
			const bedRegion &cur = tiling.getTiling()[idx];
			size_t len = cur.end() - cur.start();
			_bases[i] += len;
			if(!utilizedBefore[idx]) _totalBases += len;
			utilizedBefore[idx] = true;
		}
	}

	for(size_t i =0; i < __knownFeatures.size(); ++i) {
		static_cast<featurePayload *> (__knownFeatures[i]->getUserVariable())->totalCount(0);
	}

	_prepareAnalysis(D);
}

void EnrichmentAnalysis::beginSource(size_t s) {
	_currentFile = s;
	for(size_t i =0; i < __knownFeatures.size(); ++i) {
		featurePayload *P = static_cast<featurePayload *> (__knownFeatures[i]->getUserVariable());
		if(P) P->resetHits();
	}
	_sourceEventSizes.push_back(0);
}

void EnrichmentAnalysis::event(const bedRegion &b) {
	Genome2Feature::featureListT features;
	_genome.findFeatures(b, features, _annotatedFeatureRegex());
	if(!features.size()) return;


	if(features.size() > 1 && !_multiHit) {
		std::cerr << "Warning hit to " << b.chromosome() << ":" << b.start() << "=" << b.end()
	        	  << " hit multiple features";
		for(size_t i =0; i < features.size(); ++i) std::cerr << " " << features[i]->id();
		std::cerr << std::endl;

//		featurePayload *P = static_cast<featurePayload *> (features[0]->getUserVariable());
//		if(P) P->event();
		return;
	}

	_sourceEventSizes.back() += b.end() - b.start();
	for(Genome2Feature::featureT &F : features) {
		featurePayload *P = static_cast<featurePayload *> (F->getUserVariable());
		if(P) P->event();
	}
}

void EnrichmentAnalysis::endSource() {
	categoryListT categories;
	for(Genome2Feature::featureT &F : __knownFeatures) {
		featurePayload *P = static_cast<featurePayload *> (F->getUserVariable());
		if(P && P->hits()) {
			P->totalCount(P->totalCount() + 1);
			featureCategories(F, categories);
			for(size_t j=0; j < categories.size(); ++j) { _categoryHits[categories[j]]++; }
			++_totalHits;
			_totalEventSize += _sourceEventSizes.back();
		}
	}
}

static GULI::distribution dist;
void EnrichmentAnalysis::endAnalysis() {
	const size_t nCat  = nCategories();

	for(size_t i=0; i < nCat; ++i) {
		_pvalue[i] = dist.cdBinomial( (double) _bases[i] / _totalBases, _categoryHits[i], _totalHits);
		_enrich[i] = _categoryHits[i] / ((double) _bases[i] / _totalBases * _totalHits);
	}
	_endAnalysis();
}


