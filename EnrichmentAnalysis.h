/*
 * genericAnnotation.h
 *
 *  Created on: Oct 8, 2013
 *      Author: sven
 */

#ifndef GENERICANNOTATION_H_
#define GENERICANNOTATION_H_
#include "Genome2Feature.h"
#include "GenomeAccessibilityModel.h"

class DatasetDescriptor;
class EnrichmentAnalysis {
public:
	typedef vector<size_t> categoryListT;
	EnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A);
	virtual ~EnrichmentAnalysis();

	// Can not use virtual functions in the constructor, so, sorry, you need explicitly init the Object stack
	// right after assembling the hierarchy in order for the base object to perform some (shared) routine initialization
	// depending on details of the derived class.
	int init();

	virtual bool regionHits() { return _multiHit; }
	virtual bool regionHits(bool m) { return _multiHit = m; }
	virtual void prepareAnalysis(const DatasetDescriptor &D);
	virtual void beginSource(size_t i);
	virtual void endSource();
	virtual void endAnalysis();
	virtual void event(const GULI::bedRegion &b);


	virtual size_t nCategories() const = 0;
	double categoryPValue(size_t category) { return _pvalue[category]; }
	double categoryEnrichment(size_t category) { return _enrich[category]; }
	size_t categoryHits(size_t category)   { return _categoryHits[category];   }
	size_t categoryBases(size_t category)  { return _bases[category]; }

	virtual string  categoryId(size_t category) = 0;
	virtual string  categoryDescription(size_t category) = 0;
	virtual void    categoryFeatures(size_t category, Genome2Feature::featureListT &result) = 0;
	virtual void    featureCategories(Genome2Feature::featureT &F,  categoryListT &result) = 0;


	class featurePayload {
	public:
		featurePayload() : _count(0), _totalCount(0) {}
		virtual ~featurePayload() {}
		virtual void resetHits()      { _count = 0; }
		virtual size_t hits() const { return _count; }
		virtual void event()          { ++_count; }
		virtual size_t totalCount(size_t c)   {  return _totalCount = c;}
		virtual size_t totalCount() const { return _totalCount; }
	private:
		size_t _count, _totalCount;
	};

protected:
	Genome2Feature &_g2f() { return _genome; }
	const GenomeAccessibilityModel _acc() { return _accessibility; }
	const DatasetDescriptor & _datasetDescriptor() { return *_dataset; }

	virtual void _prepareAnalysis(const DatasetDescriptor &D) { return; }// Hook for derived objects
	virtual void _endAnalysis()                       		  { return; }// Hook for derived objects
	virtual bool _useFeature(GULI::gffDag::Node *)            { return true; }// Hook for derived objects

	virtual int _init() = 0;
	virtual void _allocateFeaturePayloads(size_t s) = 0;
	virtual featurePayload *_featurePayloadObject(size_t) = 0;

	// Overload the following method if your derived class wants something else than genes as the annotated entity
	virtual const char *_annotatedFeatureRegex() const      { return "gene"; }
	const Genome2Feature::featureListT &_knownFeatures() const 	{ return __knownFeatures; }


private:
	Genome2Feature           	&_genome;
	GenomeAccessibilityModel 	&_accessibility;
	Genome2Feature::featureListT __knownFeatures;
	const DatasetDescriptor      *_dataset;
	vector<size_t>				_bases, _categoryHits, _sourceEventSizes;
	vector<double>              _pvalue, _enrich;
	size_t 						_currentFile, _totalBases, _totalHits, _totalEventSize;
	bool 						_multiHit;
};

#endif /* GENERICANNOTATION_H_ */
