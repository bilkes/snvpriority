/*
 * GoEnrichmentAnalysis.cpp
 *
 *  Created on: Oct 9, 2013
 *      Author: sven
 */
#include <sstream>
#include <set>
#include <guli/geneOntologyXmlIo.h>
#include <guli/tabDelimited.h>
#include "GoEnrichmentAnalysis.h"


// location of the go-map file for ensembl http://useast.ensembl.org/biomart/martview
// http://useast.ensembl.org%2C%20ip-10-109-153-245.ec2.internal:8000/biomart/martview/3c88d2ef628e7e19403257cac3664839?VIRTUALSCHEMANAME=default&ATTRIBUTES=hsapiens_gene_ensembl.default.feature_page.ensembl_gene_id|hsapiens_gene_ensembl.default.feature_page.go_id&FILTERS=&VISIBLEPANEL=resultspanel
using namespace GULI;


GoEnrichmentAnalysis::GoEnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A, const char *goDef, const char *goAssoc)
   : EnrichmentAnalysis(F,A),
  _goDefintionFile(goDef),
  _goAssocFile(goAssoc)
{ }

GoEnrichmentAnalysis::~GoEnrichmentAnalysis() {}



class goAssocParser: public GULI::tabDelimited {
public:
	goAssocParser(geneOntology &go, gffDag &ensemble)
	: _go(go),
	  _ensemble(ensemble),
	  _quiet(false) {}

protected:
	void consumeData(int argc, char **argv) {
		if(argc != 2) return;
		string ensembleId = argv[0];
		string goId       = argv[1];

#ifdef RM_GOID_HEAD
		if(goId.length() < 4) {
			if(!_quiet) {
				std::cerr << "Ill formed GO-id " << argv[2] << std::endl;
				return;
			}
		}
		goId = goId.substr(3);
#endif
		if(! (_go.exists(goId) && _ensemble.exists(ensembleId)) ) {
			if(!_quiet)  {
				if (!_go.exists(goId)) {
					std::cerr << "WARNING: go " << goId << " referenced in the assoc file is unknown" << std::endl;
				} else {
					std::cerr << "WARNING: ensemble item  " << ensembleId << " referenced in the assoc file is unknown" << std::endl;
				}
			}
			return;
		}

		geneOntology::Node *goterm = _go[goId];
		gffDag::Node *feature      = _ensemble[ensembleId];

		for(geneOntology::uniqueBackwardIterator i(goterm); i != _go.root(); ++i) {
			GoEnrichmentAnalysis::GoPayload &P(*static_cast<GoEnrichmentAnalysis::GoPayload *>(i->getUserVariable()));
			P.addAnnotSingle(feature);
		}
		return;
	}
private:
	geneOntology &_go;
	gffDag       &_ensemble;
	bool _quiet;
};

int GoEnrichmentAnalysis::_init() {
	ontologyXmlIO IN;
	geneOntology::textual *t = IN.read(_goDefintionFile);
	if (!t) {
		std::cerr << "Can not parse gene ontology definition file " << _goDefintionFile << std::endl;
		return -1;
	}
	_go.textual2Node(*t);
	delete t;

	// Decorate the GO with associations
	const size_t nGo = _go.size();
	_goPayloadBuffer.resize(nGo);
	size_t idx = 0;
	for(geneOntology::uniqueForwardIterator I(_go.root()); I != _go.end(); ++I) {
		_goPayloadBuffer[idx].clearAnnot();
		_goPayloadBuffer[idx].index(idx);
		I->setUserVariable((void *) &_goPayloadBuffer[idx++]);
	}

	for(size_t i=0; i < _knownFeatures().size(); ++i) {
		static_cast<FeaturePayload *>(_knownFeatures()[i]->getUserVariable())->clearGo();
	}

	if(_goAssocFile) {
		goAssocParser P(_go, _g2f().genomeAnnotation());
		ifstream associationFile(_goAssocFile);
		if(!associationFile) {
			std::cerr << "Can not open ontology association file " << _goAssocFile << std::endl;
			return -1;
		}
		P.parse(associationFile);
		int r = associationFile.bad() ? -1 : 0;
		associationFile.close();
		if(r) return r;
	} else {

		// GFF file does not contain sufficient annotation info
		// use biomart instead
		assert(0);
/*
		// Just kept here because the code is correct (I guess) but data is not
		// usfficient, so we don't need to have this in the binary
		// extract GO association from the GFF file
		typedef GULI::gffDag::Node *featureT;
		typedef GULI::gffNodeListT featureListT;
		featureListT genes =
				GULI::gffFindFeatures(GULI::GFF_DOWN, _g2f().genomeAnnotation().root(), _annotatedFeatureRegex());;

		// Stupid (though probbaly correct), genCode associates GO entries with transcripts, not genes
		// so lets visit all transcripts for a gene
		for(featureT g : genes) {
			featureListT transcripts = GULI::gffFindFeatures(GULI::GFF_DOWN, g, "transcript");
			std::set<std::string> GO;
			for(featureT t : transcripts) {
				const char *PO = t->getAttribute("ont");
				if(!PO) continue;
				if(strlen(PO) < 4 && !strncmp(PO, "PGO:", 4)) {
					std::cerr << "Invalid Gene Ontology Annotation: " << PO
							  << "for transcript " << t->id() << std::endl;
					continue;
				}
				GO.insert(PO+1);
			}
			for(std::string goId : GO) {
				if(! _go.exists(goId) ) {
					//std::cerr << "WARNING: go " << goId << " referenced in the GFF file is unknown" << std::endl;
					continue;
				}
				geneOntology::Node *goterm = _go[goId];
				for(geneOntology::uniqueBackwardIterator i(goterm); i != _go.root(); ++i) {
					GoEnrichmentAnalysis::GoPayload &P(*static_cast<GoEnrichmentAnalysis::GoPayload *>(i->getUserVariable()));
					P.addAnnotSingle(g);
				}
			} // for(giId)
		} // for(featureT g)
	*/
	} // if(_goAssocFile)
	_goTermIds.reserve(_go.size());
	for(geneOntology::uniqueForwardIterator go(_go.root()); go != _go.end(); ++go) {
		_goTermIds.push_back(go->id());
		GoPayload *GOP = static_cast<GoPayload  *> (go->getUserVariable());
		if(!GOP) continue;
		GoPayload::annotListT &A(GOP->targetList());
		for(GoPayload::annotListT::iterator i = A.begin(); i != A.end(); ++i) {
			gffDag::Node *N = *i;
			FeaturePayload *FP = static_cast<FeaturePayload *>(N->getUserVariable());
			FP->addGo(&(*go));
		}
	}
	return 0;
}

void GoEnrichmentAnalysis::_allocateFeaturePayloads(size_t s) {
	_featurePayloadBuffer.clear();
	_featurePayloadBuffer.resize(s);
}

void GoEnrichmentAnalysis::categoryFeatures(size_t category, Genome2Feature::featureListT &result) {
	GoPayload *P = static_cast<GoPayload *>(_go[categoryId(category)]->getUserVariable());
	result.resize(P->annot().size());
	std::copy(P->annot().begin(), P->annot().end(), result.begin());
}

void GoEnrichmentAnalysis::featureCategories(Genome2Feature::featureT &F,  categoryListT &result) {
	FeaturePayload *P = static_cast<FeaturePayload *>(F->getUserVariable());
	const size_t N = P->targetList().size();
	result.resize(N);
	size_t idx=0;
	for(FeaturePayload::annotListT::iterator i=P->targetList().begin(); i != P->targetList().end(); ++i) {
		result[idx++] = static_cast<GoPayload *>((*i)->getUserVariable())->index();
	}
}

string  GoEnrichmentAnalysis::categoryDescription(size_t category) {
	string r = _go[categoryId(category)]->name() + "\t";
	Genome2Feature::featureListT features;
	categoryFeatures(category, features);
	bool first = true;
	for(size_t i=0; i < features.size(); ++i) {
		FeaturePayload *P = static_cast<FeaturePayload *>(features[i]->getUserVariable());
		if(P->totalCount()) {
			if(!first) r += ",";
			r += features[i]->attribute("Name");
			r += "(";
			std::stringstream count;
			count << P->totalCount();
			r += count.str();
			r += ")";
			first = false;
		}
	}
	r += "\t" + _go[categoryId(category)]->description();
	return r;
}
