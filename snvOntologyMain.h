/*
 * snvOntologyMain.h
 *
 *  Created on: Oct 5, 2013
 *      Author: sven
 */

#ifndef SNVONTOLOGYMAIN_H_
#define SNVONTOLOGYMAIN_H_


#include <guli/uiOperationMode.h>
#include <guli/uiParms.h>
#include <guli/gffDag.h>
#include <guli/gffAlgo.h>

class snvOntologyMain : public GULI::uiOperationMode {
	public:
		snvOntologyMain();
		virtual ~snvOntologyMain(){};
		virtual int main(int argc, char **argv);

	protected:
		typedef GULI::uiParms::Enumvar<const GULI::gffParser::parserMode *> gffSyntaxT;
		typedef GULI::uiParms::Enumvar<int> modeT;
		enum {FUSION, SNV, BED};

		GULI::uiParms      _parms;
		touchVarT          _overwrite;
		gffSyntaxT         _semantic;
		modeT              _mode;
		doublevarT		   _maxpval;
		GULI::gffDag       _geneModel;
		GULI::gffNodeListT _cdna;

	private:
		int _getGeneModel(const char *gffFileName, const GULI::gffParser::parserMode &semantic);
		static const gffSyntaxT::enumvarcfg dialectChoice[];
		static const modeT::enumvarcfg modeChoice[];
};


#endif /* TRANSCRIPTMODE_H_ */

