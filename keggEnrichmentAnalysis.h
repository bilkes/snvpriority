/*
 * keggEnrichmentAnalysis.h
 *
 *  Created on: Oct 22, 2013
 *      Author: sven
 */

#ifndef KEGGENRICHMENTANALYSIS_H_
#define KEGGENRICHMENTANALYSIS_H_

#include "EnrichmentAnalysis.h"

#include <set>
#include <guli/dagIterator.h>
#include "keggDag.h"
#include "EnrichmentAnalysis.h"

using GULI::Dag;
using GULI::keggDag;
using GULI::gffDag;


class keggEnrichmentAnalysis: public EnrichmentAnalysis {
public:
	keggEnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A, const char *keggdef, const char *keggssoc);
	virtual ~keggEnrichmentAnalysis();

	virtual size_t nCategories() const { return _categoryIds.size(); }
	virtual string  categoryId(size_t category) { return _categoryIds[category]; }
	virtual string  categoryDescription(size_t category);
	virtual void    categoryFeatures(size_t category, Genome2Feature::featureListT &result);
	virtual void    featureCategories(Genome2Feature::featureT &F,  categoryListT &result);


	keggDag &kegg() { return _kegg; }

	class FeaturePayload : public EnrichmentAnalysis::featurePayload {
	public:
		typedef std::set<keggDag::Node *> annotListT;
		virtual ~FeaturePayload() {}
		void addKegg(keggDag::Node *N) { _keggs.insert(N); }
		annotListT &targetList() { return _keggs; }
		void clearKegg() { _keggs.clear(); }
	private:
		annotListT _keggs;
	};

	class keggPayload  {
	public:
		typedef std::set<gffDag::Node *> annotListT;
		virtual ~keggPayload() {}
		void addAnnotSingle(gffDag::Node *N) {_annot.insert(N); }
		annotListT &targetList() { return _annot; }
		void clearAnnot() { _annot.clear(); }
		size_t index()  const  { return _idx; }
		size_t index(size_t i) { return _idx = i; }
		annotListT &annot() { return _annot; }
	private:
		annotListT _annot;
		size_t _idx;
	};

	protected:
	virtual void _allocateFeaturePayloads(size_t s);
	virtual featurePayload *_featurePayloadObject(size_t i) { return &_featurePayloadBuffer[i]; }
	virtual int _init();

private:
	keggDag                _kegg;
	vector<keggPayload>    _keggPayloadBuffer;
	vector<FeaturePayload> _featurePayloadBuffer;
	vector<string>         _categoryIds;
	const char              *_keggDefintionFile;
	const char              *_keggAssocFile;
};
#endif /* KEGGENRICHMENTANALYSIS_H_ */
