/*
 * GeneEnrichmentAnalysis.h
 *
 *  Created on: Oct 14, 2013
 *      Author: sven
 */

#ifndef GENEENRICHMENTANALYSIS_H_
#define GENEENRICHMENTANALYSIS_H_

#include "EnrichmentAnalysis.h"


class GeneEnrichmentAnalysis: public EnrichmentAnalysis {
public:
	GeneEnrichmentAnalysis(Genome2Feature &F, GenomeAccessibilityModel &A);
	virtual ~GeneEnrichmentAnalysis();

protected:
	class FeaturePayload : public EnrichmentAnalysis::featurePayload {
	public:
		size_t idx() { return _idx; }
		size_t idx(size_t i) { return _idx = i; }
	private:
		size_t _idx;
	};

	virtual int _init();
	virtual size_t nCategories()  const { return _knownFeatures().size(); }
	virtual void   categoryFeatures(size_t category, Genome2Feature::featureListT &result);
	virtual string  categoryId(size_t category) { return _knownFeatures()[category]->id(); }
	virtual string  categoryDescription(size_t category);
	virtual void   featureCategories(Genome2Feature::featureT &F,  categoryListT &result);


	virtual void _allocateFeaturePayloads(size_t s) {  _featurePayloadBuffer.resize(s); return; }
	virtual featurePayload *_featurePayloadObject(size_t i) { return &_featurePayloadBuffer[i]; }

private:
	vector<FeaturePayload> _featurePayloadBuffer;
};

#endif
